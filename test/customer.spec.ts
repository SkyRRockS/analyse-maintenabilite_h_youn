import { Car } from "../src/invoice/class/car";
import { Customer, Format } from "../src/invoice/class/customer";
import { Rental } from "../src/invoice/class/rental";
import { VerifyJsonAscii } from "../src/invoice/class/verifyJsonAscii";

describe('customer', () => {
    it('should create an instance of customer', () => {
        const customer = new Customer('Super Man');
        expect(customer).toBeInstanceOf(Customer);
    });
    it('should return the name of the customer', () => {
        const customer = new Customer('Iron man');
        expect(customer.getName()).toBe('Iron man');
    });
    it('should had rentals to a customer', () => {
        const customer = new Customer('Hulk');
        const car = new Car('Voiture Hulk', Car.NEW_MODEL);
        const rental = new Rental(car, 2);

        customer.addRental(rental);
        const invoice = customer.invoice(Format.ASCII);

        expect(invoice).toContain('Rental Record for Hulk');
        expect(invoice).toContain('Voiture Hulk');
        expect(invoice).toContain('Amount owed is 390.0');

    });
    it('should generate ascii invoice', () => {
        const customer = new Customer('Falcon Eye');
        const car = new Car('Voiture Falcon', Car.NEW_MODEL);
        const rental = new Rental(car, 2);

        customer.addRental(rental);
        const invoice = customer.invoice(Format.ASCII);

        expect(VerifyJsonAscii.verifyAscii(invoice)).toBe(true);
    });
    it('should generate json invoice', () => {
        const customer = new Customer('Captain America');
        const car = new Car('Voiture Captain', Car.NEW_MODEL);
        const rental = new Rental(car, 2);

        customer.addRental(rental);
        const invoice = customer.invoice(Format.JSON);

        expect(VerifyJsonAscii.verifyJson(invoice)).toBe(true);
    });
    it('should give the good amount of fidelity points', () => {
        const customer = new Customer('Thor');
        const car = new Car('Voiture Thor', Car.NEW_MODEL);
        const rental = new Rental(car, 2);

        customer.addRental(rental);
        customer.invoice(Format.JSON);

        expect(customer.getFidelityPoints()).toBe(39);
    });

});