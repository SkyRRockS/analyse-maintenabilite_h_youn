import { Calcul } from '../src/invoice/class/calcul';

class CalculsRegular implements Calcul {
    public calculAmount(days: number): number {
       let amount = 5000 + days * 9500;
       if (days > 5) {
              amount -= (days - 2) * 10000;
       }
       return amount;
    }
}

class CalculsModern implements Calcul {
    public calculAmount(days: number): number {
       let amount = 9000 + days * 15000;
       if (days > 3) {
              amount -= (days - 2) * 10000;
       }
       return amount;
    }
}

describe('calculs', () => {
    it('should return the good amount for a regular car', () => {
        const calculs = new CalculsRegular();
        expect(calculs.calculAmount(3)).toBe(33500);
        expect(calculs.calculAmount(12)).toBe(19000);
    });
    it('should return the good amount for a modern car', () => {
        const calculs = new CalculsModern();
        expect(calculs.calculAmount(1)).toBe(24000);
        expect(calculs.calculAmount(12)).toBe(89000)
    });
});