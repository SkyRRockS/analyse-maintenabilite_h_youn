import { Car } from "../src/invoice/class/car";

describe('Car', () => {
    it('should create an instance of Car', () => {
        const car = new Car('New car', Car.REGULAR);
        expect(car).toBeInstanceOf(Car);
    });
    it('should return the name of the car', () => {
        const car = new Car('New car', Car.REGULAR);
        expect(car.getTitle()).toBe('New car');
    });
    it('should return the price code we set', () => {
        const car = new Car('New car', Car.REGULAR);
        expect(car.getPriceCode()).toBe(Car.REGULAR);
    });
    it('should set the price code to the value we pass', () => {
        const car = new Car('New car', Car.REGULAR);
        car.setPriceCode(Car.NEW_MODEL);
        expect(car.getPriceCode()).toBe(Car.NEW_MODEL);
    });
});