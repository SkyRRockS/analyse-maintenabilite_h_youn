import { Car } from "../src/invoice/class/car";
import { Rental } from "../src/invoice/class/rental";

describe('rental', () => {
    it('should create an instance of rental', () => {
        const car = new Car('New car', Car.REGULAR);
        const rental = new Rental(car, 2);
        expect(rental).toBeInstanceOf(Rental);
    });
    it('should return the number of days rented', () => {
        const car = new Car('New car', Car.REGULAR);
        const rental = new Rental(car, 2);
        expect(rental.getDaysRented()).toBe(2);
    });
    it('should return the car rented', () => {
        const car = new Car('New car', Car.REGULAR);
        const rental = new Rental(car, 2);
        expect(rental.getCar()).toBeInstanceOf(Car);
    });
});