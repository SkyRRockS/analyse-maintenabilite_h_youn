import { Car } from "./invoice/class/car";
import { Customer, Format } from "./invoice/class/customer";
import { Rental } from "./invoice/class/rental";

// Exemples d'utilisation :
const Car1 = new Car("Car 1", Car.REGULAR);
const Car2 = new Car("Car 2", Car.NEW_MODEL);

const rental1 = new Rental(Car1, 3);
const rental2 = new Rental(Car2, 2);

const customer = new Customer("John Doe");
customer.addRental(rental1);
customer.addRental(rental2);

console.log(customer.invoice(Format.ASCII));
