import { Car } from "./car";
import { Rental } from "./rental";

// Format enum used to specify the format of the invoice
export enum Format {
    ASCII,
    JSON,
}

export class Customer {
    private _name: string;
    private _rentals: Rental[] = [];
    private totalAmount: {
        totalAmount: number;
        rentalsArray: {
            carTitle: string;
            amount: string;
        }[];
    } = {
        totalAmount: 0,
        rentalsArray: [],
    };

    constructor(name: string) {
        this._name = name;
    }

    public addRental(arg: Rental): void {
        this._rentals.push(arg);
    }

    public getName(): string {
        return this._name;
    }

    // The invoice method is used to generate the invoice in the specified format by using a switch case on the format parameter
    public invoice(format: Format): string {
        switch (format) {
            case Format.ASCII:
                return this.invoiceAscii();
            case Format.JSON:
                return this.invoiceJson();
        }
    }

    public invoiceAscii(): string {
        let totalAmount = 0.0;
        let frequentRenterPoints = 0;
        let result = "Rental Record for " + this.getName() + "\n";

        let rentalsArray: {
            carTitle: string;
            amount: string;
        }[] = [];

        this._rentals.forEach(each => {
            let thisAmount = 0.0;

            // Determine amounts for each line
            switch (each.getCar().getPriceCode()) {
                case Car.REGULAR:
                    thisAmount += 5000 + each.getDaysRented() * 9500;
                    if (each.getDaysRented() > 5) {
                        thisAmount -= (each.getDaysRented() - 2) * 10000;
                    }
                    break;
                case Car.NEW_MODEL:
                    thisAmount += 9000 + each.getDaysRented() * 15000;
                    if (each.getDaysRented() > 3) {
                        thisAmount -= (each.getDaysRented() - 2) * 10000;
                    }
                    break;
            }

            // Add frequent renter points
            frequentRenterPoints++;

            // Add bonus for a two day new release rental
            if (each.getCar().getPriceCode() === Car.NEW_MODEL && each.getDaysRented() > 1) {
                frequentRenterPoints++;
            }

            // Show figures for this rental
            result += "\t" + each.getCar().getTitle() + "\t" + (thisAmount / 100).toFixed(1) + "\n";
            totalAmount += thisAmount;

            rentalsArray.push({
                carTitle: each.getCar().getTitle(),
                amount: (thisAmount / 100).toFixed(1)
            });
        });

        // Add footer lines
        result += "Amount owed is " + (totalAmount / 100).toFixed(1) + "\n";
        result += "You earned " + frequentRenterPoints + " frequent renter points\n";

        this.totalAmount = {
            totalAmount: totalAmount,
            rentalsArray: rentalsArray,
        }

        return result;
    }

    public invoiceJson(): string {
        let totalAmount = 0.0;
        let frequentRenterPoints = 0;

        let rentalsArray: {
            carTitle: string;
            amount: string;
        }[] = [];

        this._rentals.forEach(each => {
            let thisAmount = 0.0;

            // Determine amounts for each line
            switch (each.getCar().getPriceCode()) {
                case Car.REGULAR:
                    thisAmount += 5000 + each.getDaysRented() * 9500;
                    if (each.getDaysRented() > 5) {
                        thisAmount -= (each.getDaysRented() - 2) * 10000;
                    }
                    break;
                case Car.NEW_MODEL:
                    thisAmount += 9000 + each.getDaysRented() * 15000;
                    if (each.getDaysRented() > 3) {
                        thisAmount -= (each.getDaysRented() - 2) * 10000;
                    }
                    break;
            }

            // Add frequent renter points
            frequentRenterPoints++;

            // Add bonus for a two day new release rental
            if (each.getCar().getPriceCode() === Car.NEW_MODEL && each.getDaysRented() > 1) {
                frequentRenterPoints++;
            }

            // Add this rental to the array
            rentalsArray.push({
                carTitle: each.getCar().getTitle(),
                amount: (thisAmount / 100).toFixed(1)
            });

            totalAmount += thisAmount;
        });

        // Create the final result object
        let result = {
            customerName: this.getName(),
            rentals: rentalsArray,
            totalAmount: (totalAmount / 100).toFixed(1),
            frequentRenterPoints: frequentRenterPoints
        };

        this.totalAmount = {
            totalAmount: totalAmount,
            rentalsArray: rentalsArray,
        }

        // Convert the result object to JSON
        return JSON.stringify(result);
    }

    public getFidelityPoints(): number {
        return Math.round(this.totalAmount.totalAmount / 1000);
    }
}
