export class Car {
    static REGULAR = 0;
    static NEW_MODEL = 1;

    private _title: string;
    private _priceCode: number;

    constructor(title: string, priceCode: number) {
        this._title = title;
        this._priceCode = priceCode;
    }

    getPriceCode(): number {
        return this._priceCode;
    }

    setPriceCode(arg: number): void {
        this._priceCode = arg;
    }

    getTitle(): string {
        return this._title;
    }
}
