export class VerifyJsonAscii {
    // This class is used to verify if a string is a valid JSON or ASCII string

    // This method verifies if a string is an ASCII string with regular expression
    public static verifyAscii(str: string): boolean {
        return /^[\x00-\x7F]*$/.test(str);
    }

    // This method verifies if a string is a valid JSON string by trying and if it fails it means it is not a valid JSON string
    public static verifyJson(str: string): boolean {
        try {
          JSON.parse(str);
          return true;
        } catch (e) {
          return false;
        }
    }
}