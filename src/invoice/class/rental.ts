import { Car } from "./car";

export class Rental {
    private _Car: Car;
    private _daysRented: number;

    constructor(Car: Car, daysRented: number) {
        this._Car = Car;
        this._daysRented = daysRented;
    }

    getDaysRented(): number {
        return this._daysRented;
    }

    getCar(): Car {
        return this._Car;
    }
}