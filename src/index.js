var Car = /** @class */ (function () {
    function Car(title, priceCode) {
        this._title = title;
        this._priceCode = priceCode;
    }
    Car.prototype.getPriceCode = function () {
        return this._priceCode;
    };
    Car.prototype.setPriceCode = function (arg) {
        this._priceCode = arg;
    };
    Car.prototype.getTitle = function () {
        return this._title;
    };
    Car.REGULAR = 0;
    Car.NEW_MODEL = 1;
    return Car;
}());
var Rental = /** @class */ (function () {
    function Rental(Car, daysRented) {
        this._Car = Car;
        this._daysRented = daysRented;
    }
    Rental.prototype.getDaysRented = function () {
        return this._daysRented;
    };
    Rental.prototype.getCar = function () {
        return this._Car;
    };
    return Rental;
}());
var Customer = /** @class */ (function () {
    function Customer(name) {
        this._rentals = [];
        this._name = name;
    }
    Customer.prototype.addRental = function (arg) {
        this._rentals.push(arg);
    };
    Customer.prototype.getName = function () {
        return this._name;
    };
    Customer.prototype.invoice = function () {
        var totalAmount = 0.0;
        var frequentRenterPoints = 0;
        var result = "Rental Record for " + this.getName() + "\n";
        this._rentals.forEach(function (each) {
            var thisAmount = 0.0;
            // Determine amounts for each line
            switch (each.getCar().getPriceCode()) {
                case Car.REGULAR:
                    thisAmount += 5000 + each.getDaysRented() * 9500;
                    if (each.getDaysRented() > 5) {
                        thisAmount -= (each.getDaysRented() - 2) * 10000;
                    }
                    break;
                case Car.NEW_MODEL:
                    thisAmount += 9000 + each.getDaysRented() * 15000;
                    if (each.getDaysRented() > 3) {
                        thisAmount -= (each.getDaysRented() - 2) * 10000;
                    }
                    break;
            }
            // Add frequent renter points
            frequentRenterPoints++;
            // Add bonus for a two day new release rental
            if (each.getCar().getPriceCode() === Car.NEW_MODEL && each.getDaysRented() > 1) {
                frequentRenterPoints++;
            }
            // Show figures for this rental
            result += "\t" + each.getCar().getTitle() + "\t" + (thisAmount / 100).toFixed(1) + "\n";
            totalAmount += thisAmount;
        });
        // Add footer lines
        result += "Amount owed is " + (totalAmount / 100).toFixed(1) + "\n";
        result += "You earned " + frequentRenterPoints + " frequent renter points\n";
        return result;
    };
    return Customer;
}());
// Exemples d'utilisation :
var Car1 = new Car("Car 1", Car.REGULAR);
var Car2 = new Car("Car 2", Car.NEW_MODEL);
var rental1 = new Rental(Car1, 3);
var rental2 = new Rental(Car2, 2);
var customer = new Customer("John Doe");
customer.addRental(rental1);
customer.addRental(rental2);
console.log(customer.invoice());
