<?php

class Car
{
    const REGULAR = 0;
    const NEW_MODEL = 1;

    private string $_title;
    private int $_priceCode;

    public function __construct(string $title, int $priceCode)
    {
        $this->_title = $title;
        $this->_priceCode = $priceCode;
    }

    public function getPriceCode(): int
    {
        return $this->_priceCode;
    }

    public function setPriceCode(int $arg): void
    {
        $this->_priceCode = $arg;
    }

    public function getTitle(): string
    {
        return $this->_title;
    }
}

class Rental
{
    private Car $_Car;
    private int $_daysRented;

    public function __construct(Car $Car, int $daysRented)
    {
        $this->_Car = $Car;
        $this->_daysRented = $daysRented;
    }

    public function getDaysRented(): int
    {
        return $this->_daysRented;
    }

    public function getCar(): Car
    {
        return $this->_Car;
    }
}

class Customer
{
    private string $_name;
    private array $_rentals = [];

    public function __construct(string $name)
    {
        $this->_name = $name;
    }

    public function addRental(Rental $arg): void
    {
        $this->_rentals[] = $arg;
    }

    public function getName(): string
    {
        return $this->_name;
    }

    public function invoice(): string
    {
        $totalAmount = 0.0;
        $frequentRenterPoints = 0;
        $result = "Rental Record for " . $this->getName() . "\n";

        foreach ($this->_rentals as $each) {
            $thisAmount = 0.0;

            // Determine amounts for each line
            switch ($each->getCar()->getPriceCode()) {
                case Car::REGULAR:
                    $thisAmount += 5000 + $each->getDaysRented() * 9500;
                    if ($each->getDaysRented() > 5) {
                        $thisAmount -= ($each->getDaysRented() - 2) * 10000;
                    }
                    break;
                case Car::NEW_MODEL:
                    $thisAmount += 9000 + $each->getDaysRented() * 15000;
                    if ($each->getDaysRented() > 3) {
                        $thisAmount -= ($each->getDaysRented() - 2) * 10000;
                    }
                    break;
            }

            // Add frequent renter points
            $frequentRenterPoints++;

            // Add bonus for a two day new release rental
            if ($each->getCar()->getPriceCode() == Car::NEW_MODEL && $each->getDaysRented() > 1) {
                $frequentRenterPoints++;
            }

            // Show figures for this rental
            $result .= "\t" . $each->getCar()->getTitle() . "\t" . number_format($thisAmount/100, 1) . "\n";
            $totalAmount += $thisAmount;
        }

        // Add footer lines
        $result .= "Amount owed is " . number_format($totalAmount/100, 1) . "\n";
        $result .= "You earned " . $frequentRenterPoints . " frequent renter points\n";

        return $result;
    }
}

// Exemples d'utilisation :
$Car1 = new Car("Car 1", Car::REGULAR);
$Car2 = new Car("Car 2", Car::NEW_MODEL);

$rental1 = new Rental($Car1, 3);
$rental2 = new Rental($Car2, 2);

$customer = new Customer("John Doe");
$customer->addRental($rental1);
$customer->addRental($rental2);

echo $customer->invoice();
