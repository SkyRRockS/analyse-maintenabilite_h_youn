# Analyse et maintenance code - HOLLANDE Youn


Ce repository est le rendu pour notre cours de maintenance et d'analyse de code.

## Tables des matières



- [Analyse et maintenance code - HOLLANDE Youn](#analyse-et-maintenance-code---hollande-youn)
  - [Tables des matières](#tables-des-matières)
  - [Exercice 1 - Questions sur les acquis, notions vues en cours](#exercice-1---questions-sur-les-acquis-notions-vues-en-cours)
  - [Exercice 2 - Refactoring](#exercice-2---refactoring)


## Exercice 1 - Questions sur les acquis, notions vues en cours

Répondre de manière succincte aux questions suivantes :

<li>
 <b>Quelles sont les principales sources de complexité dans un système logiciel (sources d’un
programme) ? </b></br></br>

Les principales sources de complexité dans un système logiciel sont les suivantes : la taille et l’échelle du code, plus le code est long et volumineux, plus il devient difficile à comprendre et à entretenir. Les interactions et les dépendances entre les éléments, les nombreuses dépendances et relations entre les modules, classes, objets et autres éléments rendent l’ensemble du système plus complexe. L’hétérogénéité technologique, telles que de multiples technologie, langage, ou ajout d’éléments tiers, et une vaste variabilité des exigences, qui entraîne des modifications fréquentes ou mal définies des besoins des utilisateurs influent également sur la complexité du code. Enfin, une mauvaise qualité du code affecte également la qualité du code écrit, sous forme de code mal structuré, mal documenté et non conforme aux meilleures pratiques.
</li>

<li>
<b>Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une
implémentation ? Vous pouvez illustrer votre réponse avec un code source minimal et/ou avec un
diagramme.</b>

<br>

Avantages de programmer vers une interface et non vers une implémentation :

**Flexibilité :** Il est plus facile de changer l'implémentation sans affecter le reste du code.

**Modularité :** Encouragement à concevoir des modules indépendants et interchangeables.

**Testabilité :** Les interfaces permettent de remplacer facilement des composants par des mocks ou des stubs pour les tests.
</li>

<li>
<b>Comment comprenez-vous chaque étape de cette heuristique ("First make it run, next make it correct, and only after that worry about making it fast") ? Comment comprenez-vous cette heuristique dans son ensemble ?</b><br>

**First make it run :** Assurez-vous que le programme fonctionne de manière basique, même s'il est imparfait ou lent. L'objectif est d'obtenir une version opérationnelle.

**Next make it correct :** Une fois que le programme fonctionne, concentrez-vous sur la correction des erreurs et la validation des résultats pour garantir qu'il fait exactement ce qui est attendu.

**Only after that worry about making it fast :** Optimer le programme après avoir testé sa validité et sa fonctionnalité. Les améliorations de performances doivent être prioritaires, tout en préservant la justesse et la fonctionnalité de version initiale.
</li>

<li>
<b>Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?<br><br></b>

**Trouver les problèmes :** Utiliser des outils pour trouver les parties du code qui ont besoin de changements.

**Planifier les changements :** Dire clairement ce qu'on veut faire et comment le faire.

**Utiliser des tests automatisés :** Mettre en place des tests unitaires et d'intégration pour s'assurer que le comportement du code reste inchangé pendant le refactoring.

**Changer de façon régulière mais minimes :** Faire des changements non majeurs mais régulier pour faciliter la validation.

**Tester souvent :** Voir souvent si les changements ne mettent pas des nouveaux problèmes.

**Valider fréquemment :** Exécuter régulièrement les tests pour vérifier que les changements n'introduisent pas de nouvelles erreurs.

## Exercice 2 - Refactoring

Lancer le projet:

```
Récuperation du projet:

git clone https://gitlab.com/SkyRRockS/analyse-maintenabilite_h_youn.git

Installation des dépendances:

npm install

Build du projet:

npm run build

Lancer le projet:

npm start

Lancer les tests:

npm test
